import "mocha";
import * as assert from "assert";

import Path, { parsePath, resolvePath } from "../../src/system/Path";

describe("resolvePath function", () => {
	it("should resolve URIs", () => {
		test("uri", "file:///C%3A/Windows/System32/shell32.dll", ".", "file:///C%3A/Windows/System32/");
		test("uri", "file:///C%3A/Windows/System32/", ".", "file:///C%3A/Windows/System32/");
		test("uri", "file:///C%3A/Windows/System32/shell32.dll", "..", "file:///C%3A/Windows/");
		test("uri", "file:///C%3A/Windows/System32/", "..", "file:///C%3A/Windows/");
		test("uri", "file:///C%3A/Windows/System32/shell32.dll", "Taskmgr.exe", "file:///C%3A/Windows/System32/Taskmgr.exe");
		test("uri", "ssh://user@hostname/hello/world/", ".", "ssh://user@hostname/hello/world/");
		test("uri", "ssh://user@hostname/hello/world/", "..", "ssh://user@hostname/hello/");
		test("uri", "ssh://user@hostname/hello/world/", "banana", "ssh://user@hostname/hello/world/banana");
	});

	it("should preserve the scheme", () => {
		test("scheme", "file:///C%3A/Windows/System32/shell32.dll", ".", "file");
		test("scheme", "file:///X%3A/Backup/", "..", "file");
		test("scheme", "file:///etc/var/", "hello", "file");

		test("scheme", "ssh://user@myserver/path/to/something.pem", ".", "ssh");
		test("scheme", "ssh://user:password@myserver/path/to/directory/", "..", "ssh");
		test("scheme", "ssh://myserver/", "banana/test", "ssh");
	});

	it("should resolve paths", () => {
		test("path", "C:\\Program Files (x86)\\Windows Defender\\shellext.dll", ".", "C:\\Program Files (x86)\\Windows Defender\\");
		test("path", "D:\\Projects\\Quick Explorer\\src\\", "../..", "D:\\Projects\\");
		test("path", "E:\\some\\path\\whatever", "/", "E:\\");
		test("path", "F:\\notDeepPath\\", "../../../../../../../../../", "F:\\");

		test("path", "/home/ubuntu/banana.png", ".", "/home/ubuntu/");
		test("path", "/home/ubuntu/", "..", "/home/");
		test("path", "/home/ubuntu/fileWith\\InName.txt", ".", "/home/ubuntu/");
		test("path", "/var/www/", "index.html", "/var/www/index.html");

		test("path", "\\\\MYSERVER\\smb-share\\file.docx", ".", "/smb-share/");
		test("path", "\\\\MYSERVER\\public\\folder\\", "..", "/public/");
		test("path", "\\\\MYSERVER\\", "banana/", "/banana/");

		test("path", "ssh://user@hostname/hello/world/", ".", "/hello/world/");
		test("path", "ssh://user@hostname/hello/world/", "..", "/hello/");
		test("path", "ssh://user@hostname/hello/world/", "banana", "/hello/world/banana");

		test("path", "file:///c:/WINDOWS/clock.avi", ".", "C:\\WINDOWS\\");
		test("path", "file://localhost/c|/WINDOWS/test/", "..", "C:\\WINDOWS\\");
		test("path", "file:///c|/WINDOWS/", "System32/", "C:\\WINDOWS\\System32\\");
		test("path", "file://localhost/c:/WINDOWS/", ".", "C:\\WINDOWS\\");
		test("path", "file:///c:/WINDOWS/", "/", "C:\\");
		test("path", "file:///d:/notDeepPath/", "../../../../../../../", "D:\\");
	});

	it("should preserve the host", () => {
		test("host", "\\\\smbserver\\banana", ".", "smbserver");
		test("host", "\\\\smbserver\\banana", "..", "smbserver");
		test("host", "\\\\smbserver\\banana", "banana", "smbserver");

		test("host", "ssh://user:password@myserver/etc", ".", "myserver");
		test("host", "ssh://user@myserver/etc", "..", "myserver");
		test("host", "ssh://myserver/etc", "banana", "myserver");

		test("host", "file://localhost/c:/WINDOWS/clock.avi", ".", "localhost");
		test("host", "file://localhost/c:/WINDOWS/", "..", "localhost");
		test("host", "file://localhost/c:/WINDOWS/", "banana", "localhost");
	});

	it("should preserve the port", () => {
		test("port", "ssh://user:password@myserver:1234/etc", ".", 1234);
		test("port", "ssh://user:password@myserver:1234/etc", "..", 1234);
		test("port", "ssh://user:password@myserver:2222/etc", "banana", 2222);
	});

	it("should preserve user info", () => {
		test("userinfo", "ssh://user:password@myserver/etc", ".", "user:password");
		test("userinfo", "ssh://user@myserver/etc", "..", "user");
		test("userinfo", "ftp://user:password@example.com/public_html/", "banana", "user:password");
	});
});

function test<K extends keyof Path>(key: K, path: string, relative: string, expected: Path[K]): void {
	assert.equal(resolvePath(parsePath(path), relative)[key], expected, `${JSON.stringify(path)} + ${JSON.stringify(relative)}`);
}
