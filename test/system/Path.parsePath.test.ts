import "mocha";
import * as assert from "assert";

import Path, { parsePath } from "../../src/system/Path";

describe("parsePath function", () => {
	it("should normalize URIs", () => {
		test("uri", "C:\\Windows/System32\\shell32.dll", "file:///C%3A/Windows/System32/shell32.dll");
	});

	it("should detect the scheme", () => {
		test("scheme", "C:/Windows\\System32\\shell32.dll", "file");
		test("scheme", "z:\\backup", "file");
		
		test("scheme", "/home/ubuntu/banana.png", "file");
		test("scheme", "/dev/null", "file");
		test("scheme", "/media/drive1/path with \\ strange & chars", "file");

		test("scheme", "\\\\MYSERVER\\smb-share\\file.docx", "smb");
		test("scheme", "\\\\MYSERVER\\public\\folder%3d/sheet.xlsx", "smb");

		test("scheme", "SSH://user:password@myserver/path/to/something.pem", "ssh");
		test("scheme", "ftp://myftpserver.com/a%20random%20folder/file.txt", "ftp");

		test("scheme", "test1:test2:test3", "test1");
	});

	it("should parse and normalize paths", () => {
		test("path", "C:\\Program Files (x86)\\Windows Defender\\shellext.dll", "C:\\Program Files (x86)\\Windows Defender\\shellext.dll");
		test("path", "C:/this\\is/something.txt", "C:\\this\\is\\something.txt");
		test("path", "c:/sp&cial_chars `'夢 all around /file ", "C:\\sp&cial_chars `'夢 all around \\file ");
		test("path", "C:\\ignore%20escape sequences.txt", "C:\\ignore%20escape sequences.txt");

		test("path", "/home/ubuntu/banana.png", "/home/ubuntu/banana.png");
		test("path", "/home/ubuntu/I<3 日本.txt", "/home/ubuntu/I<3 日本.txt");
		test("path", "/home/ubuntu/fileWith\\InName.txt", "/home/ubuntu/fileWith\\InName.txt");
		test("path", "/var/www/file with Spaces and%20escapes.html", "/var/www/file with Spaces and%20escapes.html");

		test("path", "\\\\MYSERVER\\smb-share\\file.docx", "/smb-share/file.docx");
		test("path", "\\\\MYSERVER\\public\\folder%3d/sheet.xlsx", "/public/folder%3d/sheet.xlsx");

		test("path", "ssh://user:password@myserver/path/to/something.pem", "/path/to/something.pem");
		test("path", "FTP://user:password@ftp.example.com/should%20escape%3d/everything", "/should escape=/everything");

		test("path", "file://localhost/etc/fstab", "/etc/fstab");
		test("path", "file:///etc/fstab", "/etc/fstab");
		test("path", "file:///c:/WINDOWS/clock.avi", "C:\\WINDOWS\\clock.avi");
		test("path", "FILE://localhost/c|/WINDOWS/clock.avi", "C:\\WINDOWS\\clock.avi");
		test("path", "File:///c|/WINDOWS/clock.avi", "C:\\WINDOWS\\clock.avi");
		test("path", "file://localhost/c:/WINDOWS/clock.avi", "C:\\WINDOWS\\clock.avi");
	});

	it("should identify the host", () => {
		test("host", "\\\\smbserver\\banana", "smbserver");
		test("host", "ssh://user:password@myserver/etc", "myserver");
		test("host", "file://localhost/c:/WINDOWS/clock.avi", "localhost");
		test("host", "file:///c:/WINDOWS/clock.avi", undefined);
	});

	it("should identify the port", () => {
		test("port", "ssh://user:password@myserver:22/etc", 22);
		test("port", "ftp://myserver/etc", undefined);
	});

	it("should identify user info", () => {
		test("userinfo", "ssh://user:password@myserver/etc", "user:password");
		test("userinfo", "ssh://publicserver/etc", undefined);
	});
});

function test<K extends keyof Path>(key: K, path: string, expected: Path[K]): void {
	assert.equal(parsePath(path)[key], expected, JSON.stringify(path));
}
