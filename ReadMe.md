QuickExplorer
=============

QuickExplorer is an extensible file manager with the ultimate goal to become a the only file manager needed. The 
initial goal will be to browse files and folders, and perform elementary operations; next step will be supporting 
compressed archives; remote sources (including FTP servers and Dropbox); Git integration; viewing file thumbnails; 
launching external programs by file type; UNIX file system emulation through Cygwin and MinGW; viewing/editing text 
files; viewing/manipulating images; computing and comparing file checksums; and whatever will come to my mind.



## Development

Download, install and configure [Git](https://git-scm.com/) and [Node.js](https://nodejs.org/en/) with NPM.

```
# Clone the repository
git clone git@gitlab.com:quick-explorer/quick-explorer.git

cd quick-explorer

# Install dependencies
npm install
```

I suggest using [Visual Studio Code](https://code.visualstudio.com/) to develop this software; optionally, [VS Code 
Debugger for Chrome](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome) can be 
installed to debug Electron's renderer process; Electron's main process can be debugged without any extension.

Start `dev` task to build and automatically recompile whenever something is modified. (**`CTRL` + `SHIFT` + `B`**) 
from VS Code, or `npm run dev` from command line.

Run the software with `node .` from command line or with `Electron` task from VS Code (**`F5`**). You can also run 
`Electron main` task from VS Code to skip renderer process debugging.



## Known Issues

VS Code sometimes doesn't report errors in LESS sources because 
`lessc … && echo DONE lessc|| echo DONE lessc` might print *DONE lessc* before printing output of the LESS compiler.

VS Code sometimes doesn't report TypeScript errors because parallel watching tasks are not properly sypported yet, and 
this behaviour is necessary to distinguish *main* and *script* build flows.

Breakpoints in Electron's renderer process are ignored when running scripts for the first time. This is a known 
problem in Google Chrome's remote debugger and the only known way to patch it would be to preload scripts before 
executing. Also scripts might be run before the debugger is attached.



## License

Copyright © 2016-2017 Fabio Iotti. Software released under the terms of MIT license.
