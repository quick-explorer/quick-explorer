import * as React from "react";
import { Component, MouseEvent } from "react";
import { v1 as uuid } from "uuid";
import Path, { parsePath, resolvePath } from "../../system/Path";
import ContextMenuComponent from "../contextmenu/main";

export interface Fragment {
	path: Path;
	label: string;
}

export interface FileBarProps {
	path: Path;

	onPathChange?: (path: Path) => void;
}

export interface FileBarState {
	textInputVisible: boolean;
}

export default class FileBarComponent extends Component<FileBarProps, FileBarState> {

	private readonly menuID = uuid();

	private more: HTMLButtonElement;
	private textInput: HTMLInputElement;
	private moreMenuComponent: ContextMenuComponent;

	public readonly state: FileBarState = {
		textInputVisible: false
	};

	public componentDidMount(): void {
		window.addEventListener("resize", this.handleResizeInstance);

		this.handleResize();
	}

	public componentDidUpdate(prevProps: Readonly<FileBarProps>, prevState: Readonly<FileBarState>, prevContext: any): void {
		if (this.state.textInputVisible && document.activeElement != this.textInput)
			this.textInput.focus();

		this.handleResize();
	}

	public componentWillUnmount(): void {
		window.removeEventListener("resize", this.handleResizeInstance);
	}

	public render() {
		const fragments = getFragments(this.props.path);

		return <div className="component-component-filebar">
			<div className="component-filebar-frags" hidden={this.state.textInputVisible} onClick={e => !e.isPropagationStopped() && this.setState({ textInputVisible: true })}>
				{fragments.length > 0 && [
					<FragmentComponent key="root" fragment={fragments[0]}
						onClick={() => this.props.onPathChange && this.props.onPathChange(fragments[0].path)}
					/>,
					<button ref={el => this.more = el} key="more" className="component-filebar-more" onClick={e => this.onMoreClick(e)}>
						<ContextMenuComponent ref={c => this.moreMenuComponent = c} menuID={this.menuID}>
							<menu type="context" id={this.menuID}>
								{fragments.map((fragment, i) => <menuitem key={`frag${i}`} label={fragment.label}
									onClick={() => this.props.onPathChange && this.props.onPathChange(fragment.path)}
								/>)}
							</menu>
						</ContextMenuComponent>
					</button>,
					fragments.filter((el, i) => i > 0).map((fragment, i) => <FragmentComponent key={`frag${i}`} allowHiding fragment={fragment}
						onClick={() => this.props.onPathChange && this.props.onPathChange(fragment.path)}
					/>)
				]}
			</div>
			<input ref={el => this.textInput = el} type="text" className="component-filebar-text" value={pathToString(this.props.path)} hidden={!this.state.textInputVisible} onBlur={e => this.setState({ textInputVisible: false })} />
		</div>;
	}

	private handleResizeInstance: typeof FileBarComponent.prototype.handleResize = this.handleResize.bind(this);
	private handleResize(): void {
		const fragments: Element[] = [];

		this.more.removeAttribute("hidden");

		const children = (this.more.parentElement as HTMLElement).children;
		for (let i = 0; i < children.length; i++) {
			const child = children[i];
			if (child.hasAttribute("data-allowhiding"))
				fragments.push(child);
		}

		for (const fragment of fragments) {
			fragment.setAttribute("hidden", "");
		}

		let hiddenSomething = false;
		const rootTop = ((this.more.parentElement as HTMLElement).firstElementChild as HTMLElement).getBoundingClientRect().top;
		const lastFragment = fragments[fragments.length - 1];
		for (const fragment of fragments.reverse()) {
			fragment.removeAttribute("hidden");

			if (hiddenSomething || lastFragment.getBoundingClientRect().top > rootTop) {
				hiddenSomething = true;
				fragment.setAttribute("hidden", "");
			}
		}

		if (!hiddenSomething)
			this.more.setAttribute("hidden", "");
	}

	private onMoreClick(e: MouseEvent<HTMLButtonElement>) {
		e.stopPropagation();

		if (!e.clientX && !e.clientY) {
			// because Chrome and Firefox set `clientX` and `clientY` to `0` when not available
			this.moreMenuComponent.showMenu(undefined, undefined);
		} else {
			this.moreMenuComponent.showMenu(e.clientX, e.clientY);
		}
	}
}

export interface FragmentProps {
	fragment: Fragment;
	allowHiding?: boolean;

	onMore?: () => void;
	onClick?: () => void;
}

class FragmentComponent extends Component<FragmentProps, void> {

	private more: HTMLButtonElement;

	public render() {
		return <div data-allowhiding={this.props.allowHiding} className="component-filebar-frag" tabIndex={0} onClick={e => this.onFragClick(e)}>
			<div className="component-filebar-frag-name">{this.props.fragment.label}</div>
			<button ref={el => this.more = el} className="component-filebar-frag-more" tabIndex={-1}>
			</button>
		</div>;
	}

	private onFragClick(e: MouseEvent<HTMLDivElement>) {
		e.stopPropagation();

		if (e.target == this.more || this.more.contains(e.target as Node)) {
			if (this.props.onMore)
				this.props.onMore();
		} else {
			if (this.props.onClick)
				this.props.onClick();
		}
	}
}

function pathToString(path: Path): string {
	if (!path.host && !path.port && !path.userinfo && path.scheme == "file")
		return path.path;  // "C:\WINDOWS\System32\" or "/etc/var/"
	
	if (!path.port && !path.userinfo && path.host && path.scheme == "smb")
		return `\\${path.host}${path.path.replace(/\//g, "\\")}`;
	
	return path.uri;
}

function getFragments(path: Path): Fragment[] {
	const fragments: Fragment[] = [];

	var newPath = resolvePath(path, ".");
	while (true) {
		fragments.push({ path: newPath, label: getLabel(newPath) })

		const lastPath = newPath;

		newPath = resolvePath(lastPath, "..");
		if (newPath.uri == lastPath.uri)
			break;
	}

	return fragments.reverse();
}

function getLabel(path: Path): string {
	var match: RegExpExecArray|null;
	if (path.isWindowsFormat) {
		match = /([^\\]+)[\\]$/.exec(path.path);
	} else {
		match = /([^\/]+)[\/]$/.exec(path.path);
	}

	return (match && match[1]) || "";
}
