import * as React from "react";
import { Component } from "react";
import Path from "../../system/Path";
import StatusBarComponent from "../statusbar/main";
import BrowserComponent from "../browser/main";
import ToolBarComponent from "../toolbar/main";

export interface MainViewProperties {
	leftPath: Path;
	rightPath: Path;

	onLeftPathChange?: (path: Path) => void;
	onRightPathChange?: (path: Path) => void;
}

export default class MainViewComponent extends Component<MainViewProperties, void> {

	public render() {
		return <div style={{ display: "flex", flexDirection: "column", height: "100%" }}>
			<ToolBarComponent />

			<div style={{ display: "flex", flexWrap: "wrap", flex: 1, width: "100%", height: "100%" }}>
				<div style={{ width: "50%" }}>
					<BrowserComponent path={this.props.leftPath} onPathChange={this.props.onLeftPathChange} />
				</div>
				<div style={{ width: "50%" }}>
					<BrowserComponent path={this.props.rightPath} onPathChange={this.props.onRightPathChange} />
				</div>
			</div>

			<StatusBarComponent />
		</div>;
	}
}
