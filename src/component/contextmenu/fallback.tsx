import * as React from "react";
import { Component } from "react";
import { render, unmountComponentAtNode, findDOMNode } from "react-dom";
import parseMenu, { IMenuItemOptions } from "./parser";

export function showContextMenu(menu: HTMLMenuElement, x: number, y: number): void {
	const container = document.createElement("div");
	document.body.appendChild(container);

	render(<ContextMenuComponent items={parseMenu(menu)} x={x} y={y}
		onClose={() => {
			unmountComponentAtNode(container);
			container.remove();
		}}
	/>, container);
}

interface ContextMenuProps {
	items: IMenuItemOptions[];
	x: number;
	y: number;
	onClose: () => void;
}

class ContextMenuComponent extends Component<ContextMenuProps, {}> {

	private root: HTMLDivElement|null;

	public componentDidMount(): void {
		window.addEventListener('scroll', this.closeInstance);
		window.addEventListener('resize', this.closeInstance);
		document.addEventListener('mousedown', this.onMouseDownInstance);
		document.addEventListener('touchstart', this.onMouseDownInstance);
	}

	public componentWillUnmount(): void {
		window.removeEventListener('scroll', this.closeInstance);
		window.removeEventListener('resize', this.closeInstance);
		document.removeEventListener('mousedown', this.onMouseDownInstance);
		document.removeEventListener('touchstart', this.onMouseDownInstance);
	}

	public render() {
		return <div ref={el => this.root = el}
			onContextMenu={(e) => { e.stopPropagation(); e.preventDefault(); }}
			onMouseDown={(e) => { e.stopPropagation(); e.preventDefault(); }}
			// TODO: make `onTouchStart` listener active (passive by default on recent Chrome versions).
			onTouchStart={(e) => { e.stopPropagation(); e.preventDefault(); }}
		>
			<ContextMenuSubmenuComponent items={this.props.items} onClose={this.props.onClose} x={this.props.x} y={this.props.y} />
		</div>;
	}

	private closeInstance: typeof ContextMenuComponent.prototype.close = this.close.bind(this);
	private close(): void {
		this.props.onClose();
	}

	private onMouseDownInstance: typeof ContextMenuComponent.prototype.onMouseDown = this.onMouseDown.bind(this);
	private onMouseDown(e: MouseEvent|TouchEvent): void {
		if (this.root && e.target != this.root && !this.root.contains(e.target as Node))
			this.close();
	}
}

interface ContextMenuSubmenuProps {
	items: IMenuItemOptions[];
	x: number;
	y: number;
	onClose: (explicit: boolean) => void;
}

interface ContextMenuSubmenuState {
	/** Item the cursor is currenly hovering. */
	active: number|null;
	/** Submenu that will open after a delay, if not interrupted. */
	opening: number|null;
	/** Submenu currenly open in this menu. */
	open: number|null;
	/** Force this menu's submenu to stay open even if the cursor moves outside. */
	forceOpen: boolean;
}

class ContextMenuSubmenuComponent extends Component<ContextMenuSubmenuProps, ContextMenuSubmenuState> {

	private root: HTMLDivElement|null;

	public readonly state: ContextMenuSubmenuState = {
		active: null,
		opening: null,
		open: null,
		forceOpen: false
	};

	public componentDidMount(): void {
		// if (this.root) {
		// 	this.root.focus();
		// }

		window.addEventListener('keydown', this.onKeyDownInstance);
	}

	public componentWillUnmount(): void {
		window.removeEventListener('keydown', this.onKeyDownInstance);

		this.delayOpen(null, true);
	}

	public componentDidUpdate(prevProps: Readonly<ContextMenuSubmenuProps>, prevState: Readonly<ContextMenuSubmenuState>, prevContext: any): void {
		if (this.state.opening != prevState.opening)
			this.delayOpen(this.state.opening);
	}

	public render(): JSX.Element {
		return <div ref={el => this.root = el} className="component-contextmenu" tabIndex={-1} style={{ position: "fixed", left: this.props.x, top: this.props.y }}>
			<ul className="component-contextmenu-list" role="listbox">
				{this.props.items.map((item, i) => <ContextMenuItemComponent key={i} options={item} active={!!item.enabled && (this.state.active == null ? this.state.opening : this.state.active) == i}
					onHover={() => this.setState({ active: i, opening: item.submenu ? i : null, forceOpen: false })}
					onOut={() => this.setState({ active: null })}
					onClick={() => this.clickItem(i)}
				>
					{this.state.open == i && <ContextMenuSubmenuComponent
						items={item.submenu as IMenuItemOptions[]}
						x={this.props.x + 10}  // TODO: use the correct coordinates for submenus.
						y={this.props.y + 10}
						onClose={(explicit) => { explicit ? this.props.onClose(true) : this.setState({ open: null }) }}
					/>}
				</ContextMenuItemComponent>)}
			</ul>
		</div>;
	}

	private onKeyDownInstance: typeof ContextMenuSubmenuComponent.prototype.onKeyDown = this.onKeyDown.bind(this);
	private onKeyDown(e: KeyboardEvent): void {
		if (this.state.open != null) {
			// Nothing to do if this is not the deepest open submenu.
			return;
		}

		e.preventDefault();
		e.stopPropagation();

		const activeItem = this.state.active == null ? null : this.props.items[this.state.active];

		switch (e.which) {
		case 13:  // enter
		case 32:  // spacebar
			this.state.active != null && this.clickItem(this.state.active);
			break;
		case 27:  // escape
		case 37:  // left arrow
			this.props.onClose(false);
			break;
		case 39:  // right arrow
			activeItem && activeItem.submenu && this.setState({ open: this.state.active, forceOpen: true });
			// TODO: activate first element in the opened submenu.
			break;
		case 38:  // up arrow
			this.focusNext(false);
			break;
		case 40:  // down arrow
			this.focusNext();
			break;
		}
	}

	private focusNext(directionDown = true): void {
		let i = this.state.active == null ? (directionDown ? 0 : (this.props.items.length - 1)) : (this.state.active + (directionDown ? +1 : -1));
		while (i != this.state.active) {
			const candidate = this.props.items[i];
			if (!candidate) {
				i = directionDown ? 0 : (this.props.items.length - 1);
				continue;
			}

			if (candidate.enabled) {
				this.setState({ active: i });
				break;
			}

			i += directionDown ? +1 : -1;
		}
	}

	private clickItem(i: number): void {
		const item = this.props.items[i];

		if (item.submenu) {
			this.setState({ opening: i, open: i });
		} else if (item.enabled) {
			this.props.onClose(true);
			item.click && item.click();
		}
	}

	private delayTimeoutID: NodeJS.Timer|null;
	
	/**
	 * @param immediate `true` to open immediately (cleans the timeout)
	 */
	private delayOpen(i: number|null, immediate?: boolean): void {
		if (this.delayTimeoutID != null) {
			clearTimeout(this.delayTimeoutID);
			this.delayTimeoutID = null;
		}

		if (immediate)
			this.setState({ open: i });
		else
			this.delayTimeoutID = setTimeout(() => this.setState({ open: i }), 300);
	}
}

interface ContextMenuItemProps {
	options: IMenuItemOptions;
	active: boolean;
	onHover: () => void;
	onOut: () => void;
	onClick: () => void;
}

class ContextMenuItemComponent extends Component<ContextMenuItemProps, void> {

	public render() {
		return <li role="option" tabIndex={-1}
			disabled={this.props.options.enabled == false}

			className={
				(this.props.options.type == "separator" ? "component-contextmenu-separator" : "component-contextmenu-item") + 
				(this.props.options.submenu ? " component-contextmenu-item-expandable" : "") +
				(this.props.active ? " component-contextmenu-item-hover" : "")
			}

			onMouseOver={(e) => this.props.onHover()}
			onTouchStart={(e) => this.props.onHover()}
			onTouchMove={(e) => this.props.onHover()}
			onMouseOut={(e) => this.props.onOut()}
			onClick={(e) => { e.preventDefault(); e.stopPropagation(); this.props.onClick(); }}
		>
			{!!this.props.options.label && <span className="component-contextmenu-item-label">{this.props.options.label}</span>}
			{!!this.props.options.icon && <img src={this.props.options.icon} alt="" className="component-contextmenu-item-image">{this.props.options.label}</img>}
			{this.props.children}
		</li>;
	}
}
