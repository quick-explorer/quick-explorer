import * as electron from "electron";
import parseMenu from "./parser";

type Electron = typeof electron;

export function isElectronRenderer(): boolean {
	// https://github.com/electron/electron/issues/2288
	return (window as any).process && (window as any).process.type == "renderer";
}

var electronAPI: Electron|null|undefined;
export function showElectronContextMenu(menu: HTMLMenuElement, x?: number, y?: number): void {
	if (electronAPI === undefined) {
		try {
			if ((window as any).require)
				electronAPI = (window as any)["require"]("electron");
			
			if (!electronAPI || !electronAPI.remote)
				electronAPI = null;
		} catch (e) {
			electronAPI = null;
		}
	}

	if (!electronAPI)
		throw new Error("Failed to `require(\"electron\")` to display context-menu");

	const electronMenu = new electronAPI.remote.Menu();
	for (const item of parseMenu(menu))
		electronMenu.append(new electronAPI.remote.MenuItem(item));

	// FIXME: Electron type definitions are wrong (`y` is not marked as optional).
	electronMenu.popup(electronAPI.remote.getCurrentWindow(), { x, y: y as number });
}
