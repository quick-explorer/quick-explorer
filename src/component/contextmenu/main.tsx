import * as React from "react";
import { Component } from "react";
import { findDOMNode } from "react-dom";
import { isElectronRenderer, showElectronContextMenu } from "./electron";
import { showContextMenu } from "./fallback";

export interface ContextMenuProps {
	menuID: string;
}

export default class ContextMenuComponent extends Component<ContextMenuProps, void> {

	private parent: HTMLElement|null;

	public componentDidMount(): void {
		const thisNode = findDOMNode && findDOMNode(this);
		this.parent = thisNode && thisNode.parentElement;

		this.setParentContextMenu(true);
		
		if (this.parent)
			this.parent.addEventListener('contextmenu', this.handleParentContextMenuInstance);
	}

	public componentDidUpdate(prevProps: Readonly<ContextMenuProps>, prevState: Readonly<void>, prevContext: any): void {
		if (this.props.menuID != prevProps.menuID)
			this.setParentContextMenu(true);
	}

	public componentWillUnmount(): void {
		this.setParentContextMenu(false);

		if (this.parent)
			this.parent.removeEventListener("contextmenu", this.handleParentContextMenuInstance);
	}

	public render() {
		return <noscript hidden
			onClick={e => e.stopPropagation()}  // Avoid click when clicking on menu items.
		>{this.props.children}</noscript>;
	}

	public showMenu(x?: number, y?: number): void {
		if (this.parent) {
			this.openMenuAt(x, y);
		}
	}

	private setParentContextMenu(enable: boolean): void {
		if (!this.parent)
			return;

		if (enable)
			this.parent.setAttribute("contextmenu", this.props.menuID);
		else
			this.parent.removeAttribute("contextmenu");
	}

	private handleParentContextMenuInstance: typeof ContextMenuComponent.prototype.handleParentContextMenu = this.handleParentContextMenu.bind(this);
	private handleParentContextMenu(e: PointerEvent): void {
		if (e.isTrusted && "HTMLMenuItemElement" in window)
			return;  // Supported natively, the browser will handle everything automatically.

		e.preventDefault();

		this.openMenuAt(e.clientX, e.clientY);
	}

	private openMenuAt(x?: number, y?: number): void {
		if (this.parent != null && x == null && y == null) {
			x = this.parent.offsetLeft + this.parent.offsetWidth / 2;
			y = this.parent.offsetTop + this.parent.offsetHeight / 2;
		}

		x = x || 0;
		y = y || 0;

		const menuElement = document.getElementById(this.props.menuID) as HTMLMenuElement;
		if (!menuElement)
			return;

		if (isElectronRenderer())
			return showElectronContextMenu(menuElement, x, y);  // We are in Electron, use Electron API.
		
		showContextMenu(menuElement, x, y);  // Not supported, we render it with the DOM.
	}
}
