import * as electron from "electron";

type Electron = typeof electron;
export interface IMenuItemOptions extends Electron.MenuItemConstructorOptions {
	click?: () => void;
	type?: "normal" | "separator" | "submenu"/* | "checkbox" | "radio"*/;  // TODO
	label?: string;
	sublabel?: undefined;  // TODO
	accelerator?: undefined;  // TODO
	icon?: string;
	enabled?: boolean;
	visible?: true;  // TODO
	checked?: undefined;  // TODO
	submenu?: IMenuItemOptions[];
	id?: undefined;  // TODO
	position?: undefined;  // TODO
	role?: undefined;
}

export default function parseMenu(menu: HTMLMenuElement): IMenuItemOptions[] {
	return enumerateChildren(menu).map((child) => parseMenuItem(child as HTMLElement));
}

function parseMenuItem(element: HTMLElement): IMenuItemOptions {
	switch (element.tagName.toUpperCase()) {
	case "MENUITEM":
	case "MENU":
		return {
			click: () => element.click(),
			enabled: !element.hasAttribute("disabled"),
			label: element.getAttribute("label") /*|| element.innerText*/ || undefined,
			submenu: element.children.length ?
				enumerateChildren(element).map((child) => parseMenuItem(child as HTMLElement)) :
				undefined,
			type: element.children.length ? "submenu" : "normal"
		};
	case "HR":
		return {
			type: "separator"
		};
	default:
		console.warn(`Unsupported tag <${element.tagName}> encountered while parsing <menu> tree`);
		return {
			enabled: false,
			label: `ERROR: <${element.tagName}> unsupported`
		};
	}
}

function enumerateChildren(element: Element): Element[] {
	const out: Element[] = [];
	// for (const child of element.children) {
	// 	out.push(child);
	// }

	for (let i = 0; i < element.children.length; i++) {
		out.push(element.children[i]);
	}

	return out;
}
