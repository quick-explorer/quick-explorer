import * as React from "react";
import { Component } from "react";

export default class StatusBarComponent extends Component<{}, void> {

	public render() {
		return <div className="component-statusbar">
			Hello, World!
		</div>;
	}
}
