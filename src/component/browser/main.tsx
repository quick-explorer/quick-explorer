import * as React from "react";
import { Component } from "react";
import Path from "../../system/Path";
import FileBarComponent, { Fragment } from "../filebar/main";

export interface BrowserProperties {
	path: Path;

	onPathChange?: (path: Path) => void;
}

export default class BrowserComponent extends Component<BrowserProperties, void> {

	public render() {
		return <div style={{ width: "100%", height: "100%" }}>
			<FileBarComponent
				path={this.props.path}
				onPathChange={path => this.props.onPathChange && this.props.onPathChange(path)}
			/>
		</div>;
	}
}
