import * as fs from "mz/fs";
import path from "path";
import { Readable } from "../ObjectStream";
import IPath, { resolvePath } from "../Path";
import { IResourceManager } from "../ResourceManager";

export default class FileResourceManager {

	public ls(path: IPath): Readable<IPath> {
		return new Readable<IPath>({
			objectMode: true,
			read: async function() {
				for (const file of await fs.readdir(path.path)) {
					this.push(resolvePath(path, file));
				}
			}
		});
	}
}
