import * as stream from "stream";

/* tslint:disable */

declare function Mix<T>(): { new (...args: any[]): T };

interface internal extends stream.Stream { }

declare module internal {

	interface Stream extends stream.Stream, internal { }

	export interface ReadableOptions<T> extends stream.ReadableOptions {
		objectMode: true;
		read?: (this: Readable<T>) => any;
	}

	export class Readable<T> extends stream.Readable {
		constructor(opts?: ReadableOptions<T>);
		_read(): void;
		read(): T;
		unshift(chunk: T): void;
		push(chunk: T): boolean;

		addListener(event: string, listener: Function): this;
		addListener(event: "close", listener: () => void): this;
		addListener(event: "data", listener: (chunk: T) => void): this;
		addListener(event: "end", listener: () => void): this;
		addListener(event: "readable", listener: () => void): this;
		addListener(event: "error", listener: (err: Error) => void): this;

		emit(event: string, ...args: any[]): boolean;
		emit(event: "close"): boolean;
		emit(event: "data", chunk: T): boolean;
		emit(event: "end"): boolean;
		emit(event: "readable"): boolean;
		emit(event: "error", err: Error): boolean;

		on(event: string, listener: Function): this;
		on(event: "close", listener: () => void): this;
		on(event: "data", listener: (chunk: T) => void): this;
		on(event: "end", listener: () => void): this;
		on(event: "readable", listener: () => void): this;
		on(event: "error", listener: (err: Error) => void): this;

		once(event: string, listener: Function): this;
		once(event: "close", listener: () => void): this;
		once(event: "data", listener: (chunk: T) => void): this;
		once(event: "end", listener: () => void): this;
		once(event: "readable", listener: () => void): this;
		once(event: "error", listener: (err: Error) => void): this;

		prependListener(event: string, listener: Function): this;
		prependListener(event: "close", listener: () => void): this;
		prependListener(event: "data", listener: (chunk: T) => void): this;
		prependListener(event: "end", listener: () => void): this;
		prependListener(event: "readable", listener: () => void): this;
		prependListener(event: "error", listener: (err: Error) => void): this;

		prependOnceListener(event: string, listener: Function): this;
		prependOnceListener(event: "close", listener: () => void): this;
		prependOnceListener(event: "data", listener: (chunk: T) => void): this;
		prependOnceListener(event: "end", listener: () => void): this;
		prependOnceListener(event: "readable", listener: () => void): this;
		prependOnceListener(event: "error", listener: (err: Error) => void): this;

		removeListener(event: string, listener: Function): this;
		removeListener(event: "close", listener: () => void): this;
		removeListener(event: "data", listener: (chunk: T) => void): this;
		removeListener(event: "end", listener: () => void): this;
		removeListener(event: "readable", listener: () => void): this;
		removeListener(event: "error", listener: (err: Error) => void): this;
	}

	export interface WritableOptions<T> /*extends stream.WritableOptions*/ {
		objectMode: true;
		write?: (chunk: T, encoding: string, callback: Function) => any;
		writev?: (chunks: { chunk: T, encoding: string }[], callback: Function) => any;
	}

	export class Writable<T> extends stream.Writable {
		constructor(opts?: WritableOptions<T>);
		_write(chunk: T, encoding: string, callback: Function): void;
		write(chunk: T, cb?: Function): boolean;
		write(chunk: T, encoding?: string, cb?: Function): boolean;  // Does not really make sense, but won't compile if removed.
		end(): void;
		end(chunk: T, cb?: Function): void;
		end(chunk: T, encoding?: string, cb?: Function): void;  // Does not really make sense, but won't compile if removed.

		addListener(event: string, listener: Function): this;
		addListener(event: "close", listener: () => void): this;
		addListener(event: "drain", listener: () => void): this;
		addListener(event: "error", listener: (err: Error) => void): this;
		addListener(event: "finish", listener: () => void): this;
		addListener(event: "pipe", listener: (src: Readable<T>) => void): this;
		addListener(event: "unpipe", listener: (src: Readable<T>) => void): this;

		emit(event: string, ...args: any[]): boolean;
		emit(event: "close"): boolean;
		emit(event: "drain", chunk: Buffer | string): boolean;
		emit(event: "error", err: Error): boolean;
		emit(event: "finish"): boolean;
		emit(event: "pipe", src: Readable<T>): boolean;
		emit(event: "unpipe", src: Readable<T>): boolean;

		on(event: string, listener: Function): this;
		on(event: "close", listener: () => void): this;
		on(event: "drain", listener: () => void): this;
		on(event: "error", listener: (err: Error) => void): this;
		on(event: "finish", listener: () => void): this;
		on(event: "pipe", listener: (src: Readable<T>) => void): this;
		on(event: "unpipe", listener: (src: Readable<T>) => void): this;

		once(event: string, listener: Function): this;
		once(event: "close", listener: () => void): this;
		once(event: "drain", listener: () => void): this;
		once(event: "error", listener: (err: Error) => void): this;
		once(event: "finish", listener: () => void): this;
		once(event: "pipe", listener: (src: Readable<T>) => void): this;
		once(event: "unpipe", listener: (src: Readable<T>) => void): this;

		prependListener(event: string, listener: Function): this;
		prependListener(event: "close", listener: () => void): this;
		prependListener(event: "drain", listener: () => void): this;
		prependListener(event: "error", listener: (err: Error) => void): this;
		prependListener(event: "finish", listener: () => void): this;
		prependListener(event: "pipe", listener: (src: Readable<T>) => void): this;
		prependListener(event: "unpipe", listener: (src: Readable<T>) => void): this;

		prependOnceListener(event: string, listener: Function): this;
		prependOnceListener(event: "close", listener: () => void): this;
		prependOnceListener(event: "drain", listener: () => void): this;
		prependOnceListener(event: "error", listener: (err: Error) => void): this;
		prependOnceListener(event: "finish", listener: () => void): this;
		prependOnceListener(event: "pipe", listener: (src: Readable<T>) => void): this;
		prependOnceListener(event: "unpipe", listener: (src: Readable<T>) => void): this;

		removeListener(event: string, listener: Function): this;
		removeListener(event: "close", listener: () => void): this;
		removeListener(event: "drain", listener: () => void): this;
		removeListener(event: "error", listener: (err: Error) => void): this;
		removeListener(event: "finish", listener: () => void): this;
		removeListener(event: "pipe", listener: (src: Readable<T>) => void): this;
		removeListener(event: "unpipe", listener: (src: Readable<T>) => void): this;
	}

	export interface DuplexOptions<T> extends ReadableOptions<T>, WritableOptions<T> { }

	export class Duplex<T> extends Mix<stream.Duplex & Readable<T> & Writable<T>>() {
		constructor(opts?: DuplexOptions<T>);
	}

	export interface TransformOptions<T> extends DuplexOptions<T> {
		transform?: (chunk: T, encoding: string, callback: Function) => any;
		flush?: (callback: Function) => any;
	}

	// NOTE: `Transform` lacks the `_read` and `_write` methods of `Readable`/`Writable`.
	export class Transform<T> extends Mix<stream.Transform & Duplex<T>>() {
		constructor(opts?: TransformOptions<T>);
		_transform(chunk: T, encoding: string, callback: Function): void;
	}

	export class PassThrough<T> extends Mix<stream.PassThrough & Transform<T>>() { }
}

// NOTE: this is a TypeScript hack to use `Stream` with definitions for `objectMode: true`, but is fine in JavaScript.
// This hack won't be needed anymore when type definitions for `Stream` will be fixed from Microsoft.
let b = (false as any) && internal;  // b is still considered a module with this trick
b = stream as any;

export = b;
