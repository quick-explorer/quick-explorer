import * as URI from "uri-js";

const FILE_SCHEME = "file";
const SMB_SCHEME = "smb";

const ROOT_PATH = "/";

const WINDOWS_PATH_REGEX = /^[a-z]:/i;  // "C:"
const LINUX_PATH_REGEX = /^\//;  // "/"
const UNC_PATH_REGEX = /^\\\\[^\\]/;  // "\\"

const WINDOWS_URI_PATH_REGEX = /^\/[a-z][:|]/i;  // "/c:" or "/c|"

interface IPath {
	uri: string;
	scheme: string;
	path: string;
	host?: string;
	port?: number;
	userinfo?: string;
	isWindowsFormat?: true;
}

export default IPath;

/** Parse an URI or a path in Windows, UNIX or UNC format. */
export function parsePath(path: string): IPath {
	if (WINDOWS_PATH_REGEX.test(path)) {
		const normalized = path.substring(0, 1).toUpperCase() + path.substring(1).replace(/\//g, "\\");
		return {
			uri: FILE_SCHEME + ":///" + escapeURIPath(normalized, true),
			scheme: FILE_SCHEME,
			path: normalized,
			isWindowsFormat: true
		};
	}

	if (LINUX_PATH_REGEX.test(path)) {
		return {
			uri: FILE_SCHEME + "://" + escapeURIPath(path),
			scheme: FILE_SCHEME,
			path: path
		};
	}

	if (UNC_PATH_REGEX.test(path)) {
		// https://www.iana.org/assignments/uri-schemes/prov/smb
		path = SMB_SCHEME + ":" + escapeURIPath(path, true);
	}

	const parsed = URI.parse(path, { scheme: FILE_SCHEME, tolerant: true });
	if (parsed.error) {
		throw new Error(parsed.error);
	}

	const response: IPath = {
		uri: URI.serialize(parsed, { scheme: FILE_SCHEME }),
		scheme: parsed.scheme || FILE_SCHEME,
		path: URI.unescapeComponent(parsed.path || "/"),
		userinfo: parsed.userinfo,
		host: parsed.host || undefined,
		port: typeof parsed.port === "number" ? parsed.port : undefined
	};

	if (WINDOWS_URI_PATH_REGEX.test(response.path)) {
		// "file:///c:/WINDOWS/clock.avi" is ambiguous between Windows' "C:\WINDOWS\clock.avi" and
		// Linux's "/c:/WINDOWS/clock.avi"; we go for the Windows' one.
		response.path = response.path[1].toUpperCase() + ":" + response.path.substring(3).replace(/\//g, "\\");
		response.isWindowsFormat = true;
	}

	return response;
}

export function resolvePath(path: IPath, relative: string): IPath {
	const resolved = URI.resolve(path.uri, relative, { tolerant: true });

	const parsed = parsePath(resolved);
	if (path.isWindowsFormat && parsed.path === ROOT_PATH) {
		return parsePath(resolved + path.path[0] + "%3A/");
	}

	return parsed;
}

function escapeURIPath(path: string, acceptWindowsSeparators: boolean = false): string {
	if (acceptWindowsSeparators) {
		path = path.replace(/\\/g, "/");
	}

	return path.split("/").map((c) => URI.escapeComponent(c)).join("/");
}
