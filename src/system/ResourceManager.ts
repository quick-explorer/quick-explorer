import { Readable } from "./ObjectStream";
import IPath from "./Path";

type TODO = any;  // TODO

export interface IResourceManager {
	/** Supported schemes, lowercase. */
	readonly schemes: Iterable<string>;

	/** List directory contents. */
	ls?(path: IPath): Readable<IPath>;
}
