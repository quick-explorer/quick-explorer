import * as React from "react";
import { Component } from "react";
import { render } from "react-dom";
import Path, { parsePath } from "./system/Path";
import MainViewComponent from "./component/mainview/main";

const container = document.createElement("div");
container.style.height = "100%";
document.body.appendChild(container);

interface RootState {
	leftPath: Path;
	rightPath: Path;
}

class Root extends Component<{}, RootState> {
	
	public readonly state: RootState = {
		leftPath: parsePath("C:\\Users\\User\\Desktop\\Stuff\\Textures\\Brick\\Medieval\\Bricks\\BrickOldRounded0061\\"),
		rightPath: parsePath("C:\\Users\\User\\Desktop\\Stuff\\Textures\\Brick\\Medieval\\Bricks\\BrickOldRounded0061\\")
	};

	public render() {
		return <MainViewComponent
			leftPath={this.state.leftPath}
			rightPath={this.state.rightPath}

			onLeftPathChange={path => this.setState({ leftPath: path })}
			onRightPathChange={path => this.setState({ rightPath: path })}
		/>;
	}
}

render(<Root />, container);
