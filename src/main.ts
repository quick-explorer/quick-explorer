import * as Electron from "electron";
import * as path from "path";
import * as url from "url";

// Global reference required to avoid termination on garbage collection.
let mainWindow: Electron.BrowserWindow|undefined;

function createWindow(): void {
	mainWindow = new Electron.BrowserWindow();
	mainWindow.on("closed", () => {
		mainWindow = undefined;
	});

	// Electron.Menu.setApplicationMenu(Electron.Menu.buildFromTemplate([
	// 	{ label: "TODO" }
	// ]));

	mainWindow.loadURL(url.format({
		pathname: path.join(__dirname, "index.html"),
		protocol: "file:",
		slashes: true
	}));
}

Electron.app.on("ready", createWindow);

Electron.app.on("window-all-closed", () => {
	// On OS X it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== "darwin") {
		Electron.app.quit();  // Quit when all windows are closed.
	}
});

Electron.app.on("activate", () => {
	// On OS X it"s common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (!mainWindow) {
		createWindow();
	}
});
